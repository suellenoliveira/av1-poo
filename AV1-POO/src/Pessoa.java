
public class Pessoa implements Comparable<Pessoa> {
	
	private int id;
	private String nome;
	private int idade;

	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getIdade() {
		return idade;
	}
	
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	//Construtor
	public Pessoa(int id, String nome, int idade) {
		this.id = id;
		this.nome = nome;
		this.idade = idade;
	}

	@Override
	public int compareTo(Pessoa o) {
		
		if (this.idade < o.getIdade()) {
			return -1;
		}
		if (this.idade > o.getIdade()) {
			return 1;
		}
		
		return 0;
	}
	
	

}
