import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Aplicativo {
	
	static List<Pessoa> lista = new ArrayList<Pessoa>();

    static Scanner ler = new Scanner(System.in);
	
    public static void main(String[] args) {
    	int opcao;
    	cadastrarPessoas();
    	do {
	    	System.out.println("*** MENU PRINCIPAL ***");
	        System.out.println("1-Imprimir por idade");
	        System.out.println("2-Exibir pessoa");
	        System.out.println("3-Sair");
	        System.out.println("Digite sua op��o: ");
	        opcao = ler.nextInt();
	        switch(opcao){
	        case 1: imprimirPessoas(); break;
	        case 2: consultarPessoa(); break;
	        case 3: break;
	        }
    	}while(opcao!=3);
    	
    	
    	
    }
    
    public static void cadastrarPessoas() {
		
	  	Pessoa joao = new Pessoa(1, "Joao", 10);
        lista.add(joao);
        Pessoa alice = new Pessoa(2, "Alice", 5);
        lista.add(alice);
        Pessoa fernando = new Pessoa(3, "Fernando", 27);
        lista.add(fernando);
        Pessoa carlos = new Pessoa(4, "Carlos",12);
        lista.add(carlos);
        Pessoa priscila = new Pessoa(5, "Priscila",31);
        lista.add(priscila);
    }
    
    public static void imprimirPessoas() {
    	
    	Collections.sort(lista);
    	
    	for (Pessoa item : lista) {
    		System.out.println(item.getIdade() + "-" + item.getNome() + "-" + item.getId());
    	}
    }
    
    public static void consultarPessoa() {
    	System.out.println("Digite o id: ");
    	int i = ler.nextInt();
    	
    	CachePessoa.retornaPessoa(i);
    }
}